FROM openjdk:11-jre-slim
MAINTAINER maxaalderink

EXPOSE 8080

RUN apt update && apt install -y curl && apt clean
HEALTHCHECK --start-period=10s --interval=1m --timeout=5s --retries=5 \
    CMD curl --silent --fail http://localhost:8080/actuator/health || exit 1

ADD target/*.jar app.jar

ENTRYPOINT exec java $JAVA_OPTS -jar /app.jar