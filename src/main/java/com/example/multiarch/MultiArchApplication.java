package com.example.multiarch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiArchApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultiArchApplication.class, args);
	}
}
