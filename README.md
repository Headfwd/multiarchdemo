# MultiArchDemo

Example of a CI/CD pipeline to create a multiarch Docker image.


## The Application

This is a empty Java 11 Spring Boot application that uses the Spring Boot Actuator.<br/>
The Actuator module provides several statistics REST endpoints.<br/>
In this case the health endpoint is used for health checks in the Docker image.<br/>


## The Pipeline

The pipeline is configued in the .gitlab-ci.yml.<br/>
In here 3 stages are defined:<br/>
- Compile: compile the java code to a JAR file.
- Build: build the Docker images per architecture.
- Push: push the manifest containing containing multiple arhitectures.


The jobs of these stages ar run within Docker containers based on the provided image.<br/>
On top of that the Build and Push stages require the use of a Docker service provided by docker:dind (Docker IN Docker)


The build stage has two jobs that run in parallel, one for AMD64 and ARM64 architectures.<br/>
To force the use of a custom Gitlab-Runner a tag is provided at at the bottom of the task.<br/>
This signals the Gitlab that this task should only be run on a Gitlab-Runner that has this tag.<br/>
The AMD64 tag is commented out because the default Gitlab-Runner uses AMD64 architecture and I can't assign tags to it.<br/>


In the final Push stage the Docker manifest is created.<br/>
At the moment of writing this feature is still marked as experimental and requires some extra configuration to enable.<br/>
Notice that pushing of the manifest does not take long, this is because it is only a reference to the two already existing docker images.<br/>
This means that if you want to push the manifest to a public Docker registry you must first tag&push the AMD64 and ARM64 images and create a new manifest for those. 